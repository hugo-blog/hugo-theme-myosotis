# Official Logos only

These Logos are the official versions that comply with the terms of usage.
They are only optimized and converted to the svg format.
No other modifications are made.

See:

- [LinkedIn Brand Resources](https://brand.linkedin.com/content/brand/global/en_us/index/visual-identity/logo)
  - used mark as provided
  - used variant with embedded trademark symbol (TM) as required for non-linkedin sites
  - the square with given corner radius kept as required
  - generated minified SVG from the provided version
- [GitHub Logos and Usage](https://github.com/logos)
  - used mark as provided (simplified, for small sizes)
  - the circle kept as required (no modifications, whatosoever)
  - generated minified SVG from the provided version (AI template)
- [XING logo guidelines](https://dev.xing.com/logo_rules)
  - states that the color must not be changed, used first design (dark background, monochromatic X logo)
  - asset there only given as a bitmap
  - used SVG version from [Share on XING plugin generator](https://dev.xing.com/plugins/share_button/new)
  - kept color - asked if it is possible to use b/w variant (social-plugins@xing.com)
- [GitLab Press kit](https://about.gitlab.com/press/press-kit/)
  - [GitLab Branding Usage](https://about.gitlab.com/handbook/resellers/#sts=GitLab%20Branding%20Usage)
