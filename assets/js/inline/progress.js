(function(w,d){
  w.addEventListener('DOMContentLoaded', () => {
    const dE = d.documentElement;
    let dq = d.querySelector.bind(d);
    let pE = d.getElementById('progress');
    if (pE) {
      pE = pE.style;
      d.addEventListener('scroll', () => pE.setProperty('--scroll', dE.scrollTop / (dE.scrollHeight - dE.clientHeight) * 100 + '%'));
    }
    const a = 'active';
    const m = dq('body main');
    const o = new IntersectionObserver(E => {
      E.forEach(e => {
        const i = e.target.getAttribute('id');
        let c = dq(`nav#TableOfContents a[href="#${i}"]`);
        c = c && c.parentElement.classList;
        if (c) e.intersectionRatio > 0 ? c.add(a) : c.remove(a);
      });
    },{
      rootMargin: (m ? -m.offsetTop : 0) + 'px 0px 0px'
    });
    d.querySelectorAll('section[id]').forEach(s => o.observe(s));
  });
})(window,document);
