(function(w,d){
    const l=w.location,H=l.hash,h=d.documentElement,c=h.classList,K='theme',k='data-'+K,p='#'+k,D='dark',L='light',s=localStorage,sa=function(y){var a=y?D:L,i=y?L:D;s[K]=a;h.setAttribute(k,a);c.add(a);c.remove(i);};
    var T=D===s[K]||(!(K in s)&&w.matchMedia('(prefers-color-scheme: dark)').matches);
    if(H&&H.startsWith(p)){if(p+D===H)T=true;if(p+L===H)T=false;history.pushState('',d.title,l.pathname+l.search);}
    sa(T);
    d.addEventListener('DOMContentLoaded',(e)=>{for (let a of d.querySelectorAll('a.hash-'+K))a.addEventListener('click',(e)=>(e.target.href+=p+s[K]));});
    const t=d.getElementById('t-t');
    if(t){t.checked=(D===s[K]);t.addEventListener('change',(e)=>(sa(e.target.checked)));t.parentNode.classList.remove('invisible');}
})(window,document);
