---
author: Popp Y. Seed
title: About
date: 2019-05-13
description: A short description about me.
keywords: [about, creator]
type: about
---

Poppy. A Hugo Theme using Tailwindcss JIT and daisyUI.

[Download here](https://gitlab.com/hugo-blog/hugo-theme-myosotis/).
