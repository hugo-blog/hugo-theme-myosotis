module gitlab.com/hugo-blog/hugo-theme-myosotis

go 1.19

require gitlab.com/hugo-blog/hugo-module-gdpr-privacy v0.9.0

require gitlab.com/hugo-blog/hugo-module-meta v0.6.2

require gitlab.com/hugo-blog/hugo-module-friendly-metrics v0.8.0 // indirect

require gitlab.com/hugo-blog/hugo-module-secure-resource v0.11.0 // indirect
